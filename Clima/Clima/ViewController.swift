//
//  ViewController.swift
//  Clima
//
//  Created by Christian on 25/6/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire



class ViewController: UIViewController {
    
    
    var idImg : String = ""
    
    
    

    @IBAction func Send(_ sender: Any) {
        
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(textfieldCity.text ?? "Quito")&appid=bf606df721c5f3ab20b54d79ec506fd2"
        let url = URL (string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else{
                
                print("error no data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(ChristianWeatherInfo.self, from: data)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            print(weatherInfo.weather[0].description)
            
            DispatchQueue.main.async{
                self.labelClima.text = "\(weatherInfo.weather[0].description)"
                   //idImg = "\(weatherInfo.weather[0].icon)"
                //self.labelPrron.text = "\(weatherInfo.weather[0].icon)"
                //idImg = "\(labelPrron.text)"
                let url2 = "http://openweathermap.org/img/w/\(weatherInfo.weather[0].icon).png"
                
                
                Alamofire.request(url2).responseImage { response in
                    if let image = response.result.value {
                        
                        //self.image.image = image
                        self.imageClima.image = image
                        //self.labelClima.text = "\(weatherInfo.weather[0].icon)"
                    }
                }
                
                
            }
        }
        
    
        
        
        task.resume()
    }
    


    
    
    @IBOutlet weak var labelClima: UILabel!
    
    
    @IBOutlet weak var textfieldCity: UITextField!
    
    
    @IBOutlet weak var imageClima: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

