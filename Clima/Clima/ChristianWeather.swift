//
//  ChristianWeather.swift
//  Clima
//
//  Created by Christian on 25/6/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import Foundation
import AlamofireImage
import Alamofire


struct ChristianWeatherInfo: Decodable{
    let weather: [ChristianWeather]
    
    
}

struct ChristianWeather: Decodable{
    let id:Int
    let description:String
    let icon:String
    
}
